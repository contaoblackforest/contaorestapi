<?php

define('TL_MODE', 'FE');
defined('BYPASS_TOKEN_CHECK');

require_once "../system/initialize.php";

define('TL_API_DISPLAY_EXCEPTIONS',true);
\FriendsOfContao\RestApi\Api::getInstance()->run();