<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 13:09
 */

namespace FriendsOfContao\RestApi\Controller;

use FriendsOfContao\RestApi\ApiController;

class TestController extends ApiController{

    /**
     * @param $id
     * @return array
     */
    public function getTestId($id) {
        return array('id' => $id, 'request' => $this->request->query->all());
    }

    public function postTestId($id) {

    }
} 