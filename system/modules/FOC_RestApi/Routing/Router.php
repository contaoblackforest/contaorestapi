<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 05.06.14
 * Time: 12:17
 */

namespace FriendsOfContao\RestApi\Routing;
use FriendsOfContao\RestApi\ApiException;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class Router
 * @package FriendsOfContao\Routing
 */
class Router {

    /**
     * Array of Routes
     * @var Route[]
     */
    protected static $routes = array();

    /**
     * Add a Route
     * @param Route $route
     */
    public static function addRoute(Route $route) {

        $arrMethods = $route->getMethods();

        foreach ($arrMethods as $method) {
            static::$routes[$method][$route->getName()] = &$route;
        }

    }

    /**
     * Add multiple Routes by array
     * @param Route[] $arrRoutes
     */
    public static function addRoutes(array $arrRoutes) {
        foreach ($arrRoutes as $route) {
            static::addRoute($route);
        }
    }

    /**
     * @param Request $request
     * @return Route
     * @throws \FriendsOfContao\RestApi\ApiException
     */
    public static function getMatchingRoute(Request $request) {

        foreach (static::$routes[strtolower($request->getMethod())] as $route) {

            /**
             * @var $route Route
             */
            if (!$route->match($request)) {
                continue;
            }
            return $route;
        }

        return false;

    }
} 