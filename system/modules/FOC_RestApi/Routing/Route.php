<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 05.06.14
 * Time: 12:01
 */

namespace FriendsOfContao\RestApi\Routing;
use FriendsOfContao\RestApi\Api;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Route
 * @package FriendsOfContao\Routing
 */
class Route {

    /**
     * The name of the Route
     * @var string
     */
    private $name;

    /**
     * The URL-Pattern
     * @var string
     */
    private $pattern = null;

    /**
     * Controller Class-String
     * @var string
     */
    private $controller;

    /**
     * Action-String -> must be a Method in given Controller
     * @var string
     */
    private $action;

    /**
     * Array of Parameter for calling Action
     * @var array
     */
    private $arrParams = array();

    /**
     * Allowed Methods
     * @var array
     */
    private $methods = array('get');

    /**
     * @var bool|array
     */
    private $restrictToIps = false;

    /**
     * The generated Regex-String for Check matching
     * @var string
     */
    private $regex;


    /**
     * Get a new Instance by static call
     * @param $strName
     * @return Route
     */
    public static function getInstance($strName) {
        return new self($strName);
    }

    /**
     * Set the Name
     * @param $strName
     */
    public function __construct($strName) {
        $this->name = $strName;
    }

    /**
     * Get the Name
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set the URL-Pattern
     * @param $strPattern
     * @return $this
     */
    public function setPattern($strPattern) {
        $this->pattern = $strPattern;
        return $this;
    }

    /**
     * Get the URL-Pattern
     * @return string
     */
    public function getPattern() {
        return $this->pattern;
    }

    /**
     * Set the Controller
     * @param $strController
     * @return $this
     */
    public function setController($strController) {
        $this->controller = $strController;
        return $this;
    }

    /**
     * Get the Controller
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * Set the Action
     * @param $strAction
     * @return $this
     */
    public function setAction($strAction) {
        $this->action = $strAction;
        return $this;
    }

    /**
     * Get the Action
     * @return string
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * Set HTTP-Methods
     * @param array $arrMethods
     * @return $this
     */
    public function setMethods(array $arrMethods) {
        $this->methods = $arrMethods;
        return $this;
    }

    /**
     * get HTTP-Methods
     * @return array
     */
    public function getMethods() {
        return $this->methods;
    }

    /**
     * set Restricted IPs
     * @param array $arrIps
     * @return $this
     */
    public function setRestrictedIps(array $arrIps) {
        $this->restrictToIps = $arrIps;
        return $this;
    }

    /**
     * get Parameter
     * @return array
     */
    public function getParams() {
        return $this->arrParams;
    }

    /**
     * Build the Regex form Object-Properties to check matching
     */
    private function buildRegex() {

        $this->regex = '';
        $arrSpit = preg_split("/\//",$this->pattern);
        array_shift($arrSpit);

        foreach ($arrSpit as $key => $expression) {

            if (preg_match("/^\/.*\/[a-z]+$/",$expression)) {

                $this->regex .= $expression;

            } elseif (strpos($expression,':') === 0) {

                $this->regex .= "\/[a-zA-Z-0-9]+";
                $this->arrParams[$expression] = $key;

            } else {

                $this->regex .= "\/" . $expression;
            }
        }

        $this->regex = '/'.$this->regex.'/';
    }

    /**
     * Check if Route matches to the Path in Request-Object
     * @param Request $request
     * @return bool
     */
    public function match(Request $request) {

        // no Pattern set
        if ($this->pattern == null) {
            return false;
        }

        $this->buildRegex();
        if (preg_match_all($this->regex,$request->getPathInfo())) {

            // Check IPs
            if ($this->restrictToIps !== false && !in_array($request->getClientIp(),$this->restrictToIps)) {
                return false;
            }

            $arrSplitPath = preg_split("/\//",$request->getPathInfo());
            array_shift($arrSplitPath);

            foreach ($this->arrParams as $key => $pathKey) {
                $this->arrParams[$key] = $arrSplitPath[$pathKey];
            }


            return true;
        }

        return false;
    }

} 