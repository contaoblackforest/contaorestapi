<?php




/**
 * Routing Example
 */
\FriendsOfContao\RestApi\Routing\Router::addRoute(
    \FriendsOfContao\RestApi\Routing\Route::getInstance('ShowTestController')
        ->setPattern('/test/:id')
        ->setController('FriendsOfContao\RestApi\Controller\TestController')
        ->setAction('getTestId')
        ->setMethods(array('get'))
);

\FriendsOfContao\RestApi\Routing\Router::addRoute(
    \FriendsOfContao\RestApi\Routing\Route::getInstance('ShowTestController')
        ->setPattern('/test/:id')
        ->setController('FriendsOfContao\RestApi\Controller\TestController')
        ->setAction('postTestId')
        ->setMethods(array('post'))
);

\FriendsOfContao\RestApi\Routing\Router::addRoute(
    \FriendsOfContao\RestApi\Routing\Route::getInstance('ShowTestController')
        ->setPattern('/test/:id')
        ->setController('FriendsOfContao\RestApi\Controller\TestController')
        ->setAction('putTestId')
        ->setMethods(array('put'))
);