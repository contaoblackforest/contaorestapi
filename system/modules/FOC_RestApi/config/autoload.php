<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package Foc_frontendedit
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'FriendsOfContao\RestApi',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
    // API
    'FriendsOfContao\RestApi\Api' => 'system/modules/FOC_RestApi/Api.php',
    'FriendsOfContao\RestApi\Firewall' => 'system/modules/FOC_RestApi/Firewall.php',
    'FriendsOfContao\RestApi\AccessControl' => 'system/modules/FOC_RestApi/AccessControl.php',
    'FriendsOfContao\RestApi\ApiException' => 'system/modules/FOC_RestApi/ApiException.php',
    'FriendsOfContao\RestApi\Events\ApiRequestEvent' => 'system/modules/FOC_RestApi/Events/ApiRequestEvent.php',
    'FriendsOfContao\RestApi\Events\ApiResponseEvent' => 'system/modules/FOC_RestApi/Events/ApiResponseEvent.php',


    // Router
    'FriendsOfContao\RestApi\Routing\Router' => 'system/modules/FOC_RestApi/Routing/Router.php',
    'FriendsOfContao\RestApi\Routing\Route' => 'system/modules/FOC_RestApi/Routing/Route.php',

    // Controller
    'FriendsOfContao\RestApi\ApiController' => 'system/modules/FOC_RestApi/ApiController.php',
    'FriendsOfContao\RestApi\Controller\TestController' => 'system/modules/FOC_RestApi/Controller/TestController.php',

));
