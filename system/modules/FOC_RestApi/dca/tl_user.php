<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package Core
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


$GLOBALS['TL_DCA']['tl_user']['palettes']['default'] = str_replace('{admin_legend}','{api_legend},apiSecret;{admin_legend}',$GLOBALS['TL_DCA']['tl_user']['palettes']['default']);

$GLOBALS['TL_DCA']['tl_user']['fields']['apiSecret'] = array(
    'label'                   => &$GLOBALS['TL_LANG']['tl_user']['apiSecret'],
    'exclude'                 => true,
    'inputType'               => 'text',
    'sql'                     => "varchar(64) NOT NULL default ''"
);

class tl_user_foc_api extends \Contao\Backend {

    public function __construct() {
        parent::__construct();
        $this->import('Database');
    }

    public function generateApiSecret() {

    }
}
