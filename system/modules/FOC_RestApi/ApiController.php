<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 09:27
 */

namespace FriendsOfContao\RestApi;


use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;

abstract class ApiController {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function setRequest(Request $request) {
        $this->request = $request;
    }

    /**
     * get Request Query filtered by HTTP-METHOD form Head or Body
     * @return array|mixed
     */
    protected function getRequestQueryData() {

        switch ($this->request->getMethod()) {
            case 'post':
            case 'put' :
                return json_decode($this->request->getContent(),true);
            default:
                return $this->request->query->all();
        }
    }
} 