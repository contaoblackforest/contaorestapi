<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 06.06.14
 * Time: 09:07
 */

namespace FriendsOfContao\RestApi;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiException extends \Exception {


    public function __construct($message = "", $code = 500, \Exception $previous = null) {

        parent::__construct($message, $code,$previous);
    }

    public function getResponse() {

        return new JsonResponse(
            array(
                'status' => $this->getCode(),
                'message' => $this->getMessage()
            )
        );
    }

} 