<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 05.06.14
 * Time: 10:59
 */

namespace FriendsOfContao\RestApi;

require_once TL_ROOT.'/vendor/autoload.php';

use Contao\User;
use FriendsOfContao\RestApi\Events\ApiRequestEvent;
use FriendsOfContao\RestApi\Events\ApiResponseEvent;
use FriendsOfContao\RestApi\Routing\Route;
use FriendsOfContao\RestApi\Routing\Router;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class Api extends \Contao\Controller {


    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var \Symfony\Component\EventDispatcher\EventDispatcher
     */
    private $dispatcher;

    /**
     * @var array
     */
    private static $arrPredefinedListeners = array();

    /**
     * @var Route
     */
    private $route;

    /**
     * @var \Contao\BackendUser|\Contao\FrontendUser|User
     */
    private $user;

    /**
     * Get Instance by Static-Call
     * @return Api
     */
    public static function getInstance() {
        return new self();
    }

    /**
     * Predefined EventListeners
     *
     * Gives the ability to add EventListeners in Contao´s config.php
     *
     * @param $eventName
     * @param $listener
     * @param int $priority
     */
    public static function addPredefinedEventListener($eventName, $listener, $priority = 0) {
        static::$arrPredefinedListeners[] = array($eventName, $listener, $priority);
    }

    /**
     * Initialize Api
     */
    public function __construct() {

        $this->dispatcher = new EventDispatcher();

        // Add Firewall
        $this->dispatcher->addSubscriber(new Firewall());
        // Add AccessControl
        $this->dispatcher->addSubscriber(new AccessControl());

        // register predefined Listeners
        foreach (static::$arrPredefinedListeners as $listener) {
            $this->dispatcher->addListener($listener[0],$listener[1],$listener[2]);
        }

        // call constructor of Contao\Controller
        parent::__construct();

        // Load language file
        \System::loadLanguageFile('default');

        // Set static urls
        $this->setStaticUrls();

        // Generate Symfony2 Request
        $this->request = Request::createFromGlobals();

    }

    /**
     * Run the Api
     */
    public function run() {


        try {

            // trow 204 and don´t search for Route
            if ($this->request->getPathInfo() === '/') {
                throw new ApiException('No Content',204);
            }

            // No matching Route found
            if (false == $this->route = Router::getMatchingRoute($this->request)) {
                throw new ApiException('Not found', 404);
            }

            /**
             * @var $event ApiRequestEvent
             */
            $event = $this->dispatcher->dispatch('api.request.handle',new ApiRequestEvent($this->request,$this->route));
            $this->user = $event->getUser();


            // get Response by Calling Route´s registered Controller->action()
            $response = $this->getResponse();

            /**
             * @var $event ApiResponseEvent
             */
            $event = $this->dispatcher->dispatch('api.response.send',new ApiResponseEvent($response));
            $event->getResponse()->send();
            exit;

        } catch (ApiException $e) {
            // Catch API-Exceptions and get a Response
            $e->getResponse()->send();

        } catch (\Exception $e) {

            // If defined rethrow the Exceptions
            if (TL_API_DISPLAY_EXCEPTIONS) {
                throw new \Exception($e);

            } else {
                // Catch all Other Exceptions and throw a 500 HTTP-Code
                $ae = new ApiException('System Error',500);
                $ae->getResponse()->send();
            }

        }

    }

    /**
     * get a Response
     * @return JsonResponse|Response
     * @throws ApiException
     */
    private function getResponse() {

        // get the result from Controller
        $controllerResult = $this->callController();

        // if Controller´s ReturnValue is a Response and has a Content -> return it
        if ($controllerResult instanceof Response && $controllerResult->getContent() !== '') {
            return $controllerResult;
        }

        // Generate a Response on a Bool
        if (is_bool($controllerResult)) {
            if ($controllerResult == true) {
                return new JsonResponse(null,204);
            } else {
                return new JsonResponse(null,404);
            }
        }

        // Generate a Response from a array
        if (is_array($controllerResult)) {
            return new JsonResponse($controllerResult);
        }

        // Generate a Response from a string
        if (is_string($controllerResult)) {
            return new Response($controllerResult);
        }

        // cannot generate a Response with data -> throw a 204
        throw new ApiException('No Content',204);
    }

    /**
     * Call the Controller registered in Route
     * @return mixed
     * @throws \Exception
     */
    private function callController() {

        // generate a ReflectionClass-Object
        $refClass = new \ReflectionClass($this->route->getController());

        // Couldn´t instance the Reflection Class
        if (!$refClass) {
            throw new \Exception('Controller does not exist');
        }

        if (!$refClass->getParentClass() == 'FriendsOfContao\RestApi\ApiController'){
            throw new \Exception('Controller must be a Child of FriendsOfContao\RestApi\ApiController');
        }

        // Class don´t has the Method that is defined as Action
        if (!$refClass->hasMethod($this->route->getAction())) {
            throw new \Exception('Action does not exist');
        }

        // get a Instance of ReflectionMethod
        $refMethod = $refClass->getMethod($this->route->getAction());

        // could not ge a instance
        if (!$refMethod instanceof \ReflectionMethod) {
            throw new \Exception('Action does not exist');
        }

        $refConstructor = $refClass->getConstructor();


        // Only call the Controller-Constructor if there are no Parameter set to harm Exceptions
        if ($refConstructor instanceof \ReflectionMethod && $refConstructor->getNumberOfParameters() > 0) {
            // get the instance of Controller without Constructor
            $objController = $refClass->newInstanceWithoutConstructor();
        } else {
            // get the instance with calling the Constructor
            $objController = $refClass->newInstance();
        }

        $objController->setRequest($this->request);

        try {
            // run the Controller Action
            return $refMethod->invokeArgs($objController,$this->route->getParams());
        } catch (\Exception $e) {

            // Check if API displays Exceptions
            if (TL_API_DISPLAY_EXCEPTIONS) {
                // throw the Exception again
                throw $e;
            } else {
                // throw a ApiException with HTTP-CODE 500
                throw new ApiException('SystemError');
            }

        }

    }

} 