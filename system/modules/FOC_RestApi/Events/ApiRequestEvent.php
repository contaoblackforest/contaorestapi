<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 10:11
 */

namespace FriendsOfContao\RestApi\Events;


use Contao\User;
use FriendsOfContao\RestApi\Routing\Route;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class ApiRequestEvent extends Event {

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * @var \Contao\BackendUser|\Contao\FrontendUser|\Contao\User
     */
    private $user;

    /**
     * @var \FriendsOfContao\RestApi\Routing\Route
     */
    private $route;

    /**
     * @param Request $request
     * @param Route $route
     */
    public function __construct(Request $request, Route $route) {
        $this->request = $request;
        $this->route = $route;
    }

    /**
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * @return Route
     */
    public function getRoute() {
        return $this->route;
    }

    /**
     * @return \Contao\BackendUser|\Contao\FrontendUser|User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param \Contao\BackendUser|\Contao\FrontendUser|User $user
     */
    public function setUser(User $user) {
        $this->user = $user;
    }
}