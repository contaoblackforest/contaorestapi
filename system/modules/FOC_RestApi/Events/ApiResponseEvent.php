<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 10:11
 */

namespace FriendsOfContao\RestApi\Events;


use FriendsOfContao\RestApi\Api;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApiResponseEvent extends Event {

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $response;

    /**
     * @param Response $response
     */
    public function __construct(Response $response) {
        $this->response = $response;
    }

    /**
     * @return Response
     */
    public function getResponse() {
        return $this->response;
    }
}