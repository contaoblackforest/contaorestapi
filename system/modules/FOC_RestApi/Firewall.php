<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 10:23
 */

namespace FriendsOfContao\RestApi;


use Contao\BackendUser;
use Contao\Controller;
use Contao\FrontendUser;
use FriendsOfContao\RestApi\Events\ApiRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

class Firewall extends Controller implements EventSubscriberInterface {

    /**
     * Subscribe Firewall to EventDispatcher
     * @return array
     */
    public static function getSubscribedEvents() {
        return array(
            'api.request.handle' => array('onHandleRequest', 9999)
        );
    }

    /**
     * Init Firewall
     */
    public function __construct() {

        parent::__construct();

        $this->import('BackendUser');
        $this->BackendUser->authenticate();

        $this->import('FrontendUser');
        $this->FrontendUser->authenticate();

    }

    /**
     * invoked on api.request.handle Event
     * @throws ApiException
     */
    public function onHandleRequest(ApiRequestEvent $event) {

        if ($user = $this->authenticate()) {
            $event->setUser($user);
        } else {
            // no User could be Authenticated -> throw the Exception
            throw new ApiException('Forbidden',403);
        }
    }

    private function authenticate() {

        if ($this->BackendUser->id) {
            return $this->BackendUser;
        }

        if ($this->FrontendUser->id) {
            return $this->FrontendUser;
        }

        return false;
    }
} 