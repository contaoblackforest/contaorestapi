<?php
/**
 * Created by PhpStorm.
 * User: dtomasi
 * Date: 24.06.14
 * Time: 11:06
 */

namespace FriendsOfContao\RestApi;


use Contao\Controller;
use Contao\User;
use FriendsOfContao\RestApi\Events\ApiRequestEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

class AccessControl extends Controller implements EventSubscriberInterface {


    /**
     * Subscribe Firewall to EventDispatcher
     * @return array
     */
    public static function getSubscribedEvents() {
        return array(
            'api.request.handle' => array('onHandleRequest', 9998)
        );
    }

    /**
     * Make Controllers Constructor public
     */
    public function __construct() { parent::__construct(); }

    /**
     * Invoked on api.request.handle Event
     * @throws ApiException
     */
    public function onHandleRequest(ApiRequestEvent $event) {

        if (!$this->authorize($event->getUser(),$event->getRequest())) {
            throw new ApiException('Unauthorized',403);
        }
    }

    /**
     * Check the Permissions
     * @param User $user
     * @param Request $request
     * @return bool
     */
    private function authorize(User $user,Request $request) {

        //@todo Implement authorization-logic

        return true;
    }
} 